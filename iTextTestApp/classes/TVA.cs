﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iTextTestApp.classes
{
    public class TVA
    {
        public int Id { get; set; }
        public decimal Taux { get; set; }
    }
}
