﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iText.IO.Image;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Kernel.Colors;
using iText.Kernel.Pdf.Canvas.Draw;
using iText.Layout.Properties;
using iTextTestApp.classes;
using iText.Kernel.Pdf.Colorspace;
using Image = iText.Layout.Element.Image;

namespace iTextTestApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnPdf_Click(object sender, EventArgs e)
        {
            //
            // Preparing Data
            //

            // Client
            Client client = new Client
            {
                Civilité = "Mr",
                Nom = "RIEHL",
                Prénom = "DAVID",
                Adresse = "16 place de la République",
                CodePostal = "59300",
                Ville = "VALENCIENNES",
                Telephone = "",
                Mobile = "0651461632",
                Compte = "",
            };

            // Vehicule
            Vehicule vehicule = new Vehicule
            {
                DateOR = new DateTime(2018, 12, 20),
                Kms = 16024,
                Tvv = "JSAY",
                Gamme = "LODGY",
                Modele = "SRY7 12H 6",
                Immatriculation = "ES568QM",
                NumSérieFabrication = "UU1JSDDYG59254488/I208183",
                Date1ereMEC = new DateTime(2017, 12, 11),
                DateLivr = new DateTime(2017, 12, 29),
                Client = client,
            };

            // TVA
            TVA tva2 = new TVA { Id = 2, Taux = 20m };

            // Elements
            Dictionary<Element, ElementFacture> elements = new Dictionary<Element, ElementFacture>();
            // Element
            elements.Add(
                new Element
                {
                    Reference = "0048",
                    Designation = "OG REVISION VEHICULE",
                    Type = TypeElement.MO,
                    TVA = tva2
                },
                new ElementFacture
                {
                    Quantité = 1.30m,
                    PrixHTFacture = 45.00m,
                    Remise = 0.00m,
                }
            );

            // Element
            elements.Add(
                new Element
                {
                    Reference = "6170",
                    Designation = "OG REMPL FILTRE HABITACLE",
                    Type = TypeElement.MO,
                    TVA = tva2
                },
                new ElementFacture
                {
                    Quantité = 0.20m,
                    PrixHTFacture = 45.00m,
                    Remise = 0.00m,
                }
            );

            // Element
            elements.Add(
                new Element
                {
                    Reference = "3123",
                    Designation = "PERMUTATION DES 4 PNEUS ETE/HIVER",
                    Type = TypeElement.MO,
                    TVA = tva2
                },
                new ElementFacture
                {
                    Quantité = 1.00m,
                    PrixHTFacture = 45.00m,
                    Remise = 0.00m,
                }
            );

            // Element
            elements.Add(
                new Element
                {
                    Reference = "152095084R",
                    Designation = "CARTOUCHE FILTRE HUILE",
                    Type = TypeElement.Piece,
                    TVA = tva2
                },
                new ElementFacture
                {
                    Quantité = 1.00m,
                    PrixHTFacture = 14.33m,
                    Remise = 0.00m,
                }
            );

            // Element
            elements.Add(
                new Element
                {
                    Reference = "VX500R",
                    Designation = "YACCO VX500R 10W40",
                    Type = TypeElement.Piece,
                    TVA = tva2
                },
                new ElementFacture
                {
                    Quantité = 1.00m,
                    PrixHTFacture = 4.81m,
                    Remise = 0.00m,
                }
            );

            // Element
            elements.Add(
                new Element
                {
                    Reference = "8200641648",
                    Designation = "RONDELLE ETANCHEITE",
                    Type = TypeElement.Piece,
                    TVA = tva2
                },
                new ElementFacture
                {
                    Quantité = 1.00m,
                    PrixHTFacture = 2.10m,
                    Remise = 0.00m,
                }
            );

            // Element
            elements.Add(
                new Element
                {
                    Reference = "272773277R",
                    Designation = "FILTRE HABITACLE",
                    Type = TypeElement.Piece,
                    TVA = tva2
                },
                new ElementFacture
                {
                    Quantité = 1.00m,
                    PrixHTFacture = 22.92m,
                    Remise = 0.00m,
                }
            );

            // Element
            elements.Add(
                new Element
                {
                    Reference = "7711238969",
                    Designation = "LAVE-GLACE HIVER",
                    Type = TypeElement.Piece,
                    TVA = tva2
                },
                new ElementFacture
                {
                    Quantité = 1.00m,
                    PrixHTFacture = 2.58m,
                    Remise = 0.00m,
                }
            );

            // Element
            elements.Add(
                new Element
                {
                    Reference = "7711771317",
                    Designation = "MICHELIN 195/55 R16 91H E B 1 68 ALPIN 5 XL",
                    Type = TypeElement.Piece,
                    TVA = tva2
                },
                new ElementFacture
                {
                    Quantité = 4.00m,
                    PrixHTFacture = 122.00m,
                    Remise = 25.00m,
                }
            );

            // Element
            elements.Add(
                new Element
                {
                    Reference = "DECH",
                    Designation = "PARTICIPATION RECYCLAGE DES DECHETS",
                    Type = TypeElement.Piece,
                    TVA = tva2
                },
                new ElementFacture
                {
                    Quantité = 1.00m,
                    PrixHTFacture = 2.00m,
                    Remise = 0.00m,
                }
            );

            // Facture
            Facture facture = new Facture
            {
                DateFacturation = new DateTime(2018, 12, 20),
                NumeroOrdre = 113923,
                NumeroActivité = 1,
                Accueil = "A renseigner",
                DatePaiement = new DateTime(2018, 12, 20),
                Vehicule = vehicule,
                Elements = elements,
            };

            //
            // Initialize PDF Document
            //
            string file = @"test.pdf";
            PdfWriter writer = new PdfWriter(file);
            PdfDocument pdf = new PdfDocument(writer);
            Document document = new Document(pdf);
            Paragraph paragraph;
            Table table;
            Cell cell;

            int page = 1;

            //
            // SPECIMEN
            //

            paragraph = new Paragraph("SPECIMEN")
                .SetFontSize(72)
                .SetFontColor(ColorConstants.LIGHT_GRAY)
                .SetBold()
                .SetRotationAngle(45 * Math.PI / 180)
                .SetOpacity(0.3f)
                .SetFixedPosition(200, 200, 500);
            document.Add(paragraph);

            //
            // ENTETE
            //

            // Logo
            string imgFile = "ressources/img/128px-RENAULT_LOGO.png";
            Image image = new Image(ImageDataFactory.Create(imgFile), 450, 780);
            document.Add(image);
            
            // First paragraph
            paragraph = new Paragraph();
            _ = paragraph.Add("S.A.S. GARAGE RIZZO" + Environment.NewLine)
                         .Add("SPECIALISTE RENAULT SPORT" + Environment.NewLine)
                         .SetMargin(0)
                         .SetMultipliedLeading(1.0f)
                         .AddStyle(new Style().SetBold().SetFontSize(11f));
            document.Add(paragraph);
            
            // second paragraph
            paragraph = new Paragraph();
            _ = paragraph.Add("CAPITAL DE 125 000 €" + Environment.NewLine)
                         .Add("24 RUE ERNEST MACAREZ" + Environment.NewLine)
                         .Add("59300 VALENCIENNES" + Environment.NewLine)
                         .Add("Tél.: 03.27.46.18.85 - Fax: 03.27.36.07.20" + Environment.NewLine)
                         .Add("Mail:" + Environment.NewLine)
                         .Add("SIRET: 34288460800023 / APE: 4511Z" + Environment.NewLine)
                         .Add("N° TVA intra-communautaire : FR70342884608" + Environment.NewLine)
                         .SetMarginTop(0)
                         .SetMultipliedLeading(1.0f)
                         .AddStyle(new Style().SetFontSize(10f));
            document.Add(paragraph);
            
            //
            // N° Facture
            //
            paragraph = new Paragraph();
            _ = paragraph.Add("Facture N° 354780")
                         .SetMarginTop(7f)
                         .SetMarginBottom(7f)
                         .AddStyle(new Style().SetBold().SetFontSize(14f));
            document.Add(paragraph);
            
            //
            // Ordre de Réparation
            //
            
            // first paragraph
            paragraph = new Paragraph();
            _ = paragraph.Add("du 20/12/2018 - Exemplaire client")
                         .SetMarginBottom(0)
                         .AddStyle(new Style().SetBold().SetFontSize(10f));
            document.Add(paragraph);
            
            // second paragraph
            paragraph = new Paragraph();
            _ = paragraph.Add("N° O.R. : 113923   / Activité : 001" + Environment.NewLine)
                         .Add("Accueilli par : A renseigner" + Environment.NewLine)
                         .Add("N° Compte :" + Environment.NewLine)
                         .Add("Payable le 20/12/2018" + Environment.NewLine)
                         .SetMarginTop(0)
                         .SetMarginBottom(30f)
                         .SetMultipliedLeading(1.1f)
                         .AddStyle(new Style().SetFontSize(9f));
            document.Add(paragraph);
            
            //
            // Client
            //
            
            // first paragraph
            paragraph = new Paragraph();
            _ = paragraph.Add("Mr RIEHL DAVID")
                         .SetMarginBottom(0)
                         .SetFixedPosition(350, 695, 200)
                         .AddStyle(new Style().SetBold().SetFontSize(12f).SetItalic());
            document.Add(paragraph);
            
            // second paragraph
            paragraph = new Paragraph();
            _ = paragraph.Add("16 place de la République" + Environment.NewLine)
                         .Add("59300 VALENCIENNES" + Environment.NewLine)
                         .SetMarginTop(0)
                         .SetFixedPosition(350, 665, 200)
                         .AddStyle(new Style().SetFontSize(10f));
            document.Add(paragraph);
            
            // third paragraph
            paragraph = new Paragraph();
            _ = paragraph.Add("Tel:                          / Mobile:" + Environment.NewLine)
                         .SetFixedPosition(350, 580, 200)
                         .AddStyle(new Style().SetFontSize(8f));
            document.Add(paragraph);

            //
            // Line Separator
            //
            document.Add(new LineSeparator(new SolidLine(0.5f)).SetMarginBottom(1f));

            //
            // Tableau Véhicule
            //

            List<string> tableHeaders = new List<string>()
            {
                "Page", "Date O.R.", "KMS", "T.V.V", "Gamme", "Modèle", "Immat.",
                "Numéro de série/Fabrication", "1ère MEC", "Date livr."
            };

            table = new Table(tableHeaders.Count).SetFontSize(8f);

            //
            // Table Head
            //

            foreach(string header in tableHeaders)
            {
                cell = new Cell()
                    .SetBorderLeft(null)
                    .SetBorderTop(null)
                    .SetBorderRight(null)
                    .SetPaddingBottom(0)
                    .Add(new Paragraph(header).SetBold().SetMarginBottom(0));
                table.AddCell(cell);
            }

            //
            // Table Body
            //

            // Page
            cell = new Cell()
                .SetBorderLeft(null)
                .SetBorderTop(null)
                .SetBorderRight(null)
                .SetPaddingBottom(0)
                .SetTextAlignment(TextAlignment.CENTER)
                .Add(new Paragraph(page.ToString()).SetMarginBottom(0));
            table.AddCell(cell);

            // Date O.R.
            cell = new Cell()
                .SetBorderLeft(null)
                .SetBorderTop(null)
                .SetBorderRight(null)
                .SetPaddingBottom(0)
                .SetPaddingRight(12)
                .Add(new Paragraph(vehicule.DateOR.ToString("dd/MM/yyyy")).SetMarginBottom(0));
            table.AddCell(cell);

            // Kms
            cell = new Cell()
                .SetBorderLeft(null)
                .SetBorderTop(null)
                .SetBorderRight(null)
                .SetPaddingBottom(0)
                .SetPaddingRight(18)
                .Add(new Paragraph(vehicule.Kms.ToString("##,#")).SetMarginBottom(0));
            table.AddCell(cell);

            // T.V.V
            cell = new Cell()
                .SetBorderLeft(null)
                .SetBorderTop(null)
                .SetBorderRight(null)
                .SetPaddingBottom(0)
                .SetPaddingRight(10)
                .Add(new Paragraph(vehicule.Tvv).SetMarginBottom(0));
            table.AddCell(cell);

            // Gamme
            cell = new Cell()
                .SetBorderLeft(null)
                .SetBorderTop(null)
                .SetBorderRight(null)
                .SetPaddingBottom(0)
                .SetPaddingRight(20)
                .Add(new Paragraph(vehicule.Gamme).SetMarginBottom(0));
            table.AddCell(cell);

            // Modele
            cell = new Cell()
                .SetBorderLeft(null)
                .SetBorderTop(null)
                .SetBorderRight(null)
                .SetPaddingBottom(0)
                .SetPaddingRight(6)
                .Add(new Paragraph(vehicule.Modele).SetMarginBottom(0));
            table.AddCell(cell);

            // Immatriculation
            cell = new Cell()
                .SetBorderLeft(null)
                .SetBorderTop(null)
                .SetBorderRight(null)
                .SetPaddingBottom(0)
                .SetPaddingRight(20)
                .Add(new Paragraph(vehicule.Immatriculation).SetMarginBottom(0));
            table.AddCell(cell);

            // Numéro de Série / Fabrication
            cell = new Cell()
                .SetBorderLeft(null)
                .SetBorderTop(null)
                .SetBorderRight(null)
                .SetPaddingBottom(0)
                .Add(new Paragraph(vehicule.NumSérieFabrication).SetMarginBottom(0));
            table.AddCell(cell);

            // 1ère MEC
            cell = new Cell()
                .SetBorderLeft(null)
                .SetBorderTop(null)
                .SetBorderRight(null)
                .SetPaddingBottom(0)
                .Add(new Paragraph(vehicule.Date1ereMEC.ToString("dd/MM/yyyy")).SetMarginBottom(0));
            table.AddCell(cell);

            // Livraison
            cell = new Cell()
                .SetBorderLeft(null)
                .SetBorderTop(null)
                .SetBorderRight(null)
                .SetPaddingBottom(0)
                .Add(new Paragraph(vehicule.DateLivr.ToString("dd/MM/yyyy")).SetMarginBottom(0));
            table.AddCell(cell);

            //
            // End of Table
            //
            document.Add(table);

            //
            // Tableau Elements Facturés
            //

            table = new Table(8)
                .SetMarginTop(2)
                .SetMarginBottom(12)
                .SetFontSize(8f);

            //
            // Table Head
            //

            // Désignation
            cell = new Cell()
                .SetBorderTop(null).SetBorderLeft(null).SetBorderRight(null).SetBorderBottom(null)
                .SetWidth(195f)
                .SetPaddingBottom(0)
                .SetFontSize(9)
                .Add(new Paragraph("Désignation").SetBold().SetMarginBottom(0));
            table.AddCell(cell);

            // Référence
            cell = new Cell()
                .SetBorderTop(null).SetBorderLeft(null).SetBorderRight(null).SetBorderBottom(null)
                .SetWidth(60f)
                .SetPaddingBottom(0)
                .SetTextAlignment(TextAlignment.CENTER)
                .SetFontSize(9f)
                .Add(new Paragraph("Référence").SetBold().SetMarginBottom(0));
            table.AddCell(cell);

            // Quantité
            cell = new Cell()
                .SetBorderTop(null).SetBorderLeft(null).SetBorderRight(null)
                .SetPaddingBottom(0)
                .SetTextAlignment(TextAlignment.CENTER)
                .SetFontSize(9f)
                .Add(new Paragraph("Qté/Tps").SetBold().SetMarginBottom(0));
            table.AddCell(cell);

            // Prix Unitaire HT
            cell = new Cell()
                .SetBorderTop(null).SetBorderLeft(null).SetBorderRight(null)
                .SetPaddingBottom(0)
                .SetTextAlignment(TextAlignment.CENTER)
                .SetFontSize(9f)
                .Add(new Paragraph("Prix unit. HT").SetBold().SetMarginBottom(0));
            table.AddCell(cell);

            // Remise
            cell = new Cell()
                .SetBorderTop(null).SetBorderLeft(null).SetBorderRight(null)
                .SetPaddingBottom(0)
                .SetTextAlignment(TextAlignment.CENTER)
                .SetFontSize(9f)
                .Add(new Paragraph("% R").SetBold().SetMarginBottom(0));
            table.AddCell(cell);

            // Prix Unitaire Net HT
            cell = new Cell()
                .SetBorderTop(null).SetBorderLeft(null).SetBorderRight(null)
                .SetPaddingBottom(0)
                .SetTextAlignment(TextAlignment.CENTER)
                .SetFontSize(9f)
                .Add(new Paragraph("Prix unit. net HT").SetBold().SetMarginBottom(0));
            table.AddCell(cell);

            // Montant HT
            cell = new Cell()
                .SetBorderTop(null).SetBorderLeft(null).SetBorderRight(null)
                .SetPaddingBottom(0)
                .SetTextAlignment(TextAlignment.CENTER)
                .SetFontSize(9f)
                .Add(new Paragraph("Montant H.T.").SetBold().SetMarginBottom(0));
            table.AddCell(cell);

            // TVA
            cell = new Cell()
                .SetBorderTop(null).SetBorderLeft(null).SetBorderRight(null)
                .SetPaddingBottom(0)
                .SetTextAlignment(TextAlignment.CENTER)
                .SetFontSize(9f)
                .Add(new Paragraph("T").SetBold().SetMarginBottom(0));
            table.AddCell(cell);

            //
            // Table Body
            //

            foreach (KeyValuePair<Element, ElementFacture> ligne in elements)
            {
                Element element = (Element)ligne.Key;
                ElementFacture elementFacture = (ElementFacture)ligne.Value;

                // Désignation
                cell = new Cell()
                    .SetBorderTop(null).SetBorderLeft(null).SetBorderRight(null).SetBorderBottom(null)
                    .Add(new Paragraph(element.Designation).SetMarginBottom(0));
                table.AddCell(cell);

                // Référence
                cell = new Cell()
                    .SetBorderTop(null).SetBorderLeft(null).SetBorderRight(null).SetBorderBottom(null)
                    .Add(new Paragraph(element.Reference).SetMarginBottom(0));
                table.AddCell(cell);

                // Quantité / Temps
                cell = new Cell()
                    .SetBorderTop(null).SetBorderLeft(null).SetBorderRight(null).SetBorderBottom(null)
                    .SetTextAlignment(TextAlignment.CENTER)
                    .Add(new Paragraph(elementFacture.Quantité.ToString("0.00")).SetMarginBottom(0));
                table.AddCell(cell);

                // Prix Unitaire HT
                cell = new Cell()
                    .SetBorderTop(null).SetBorderLeft(null).SetBorderRight(null).SetBorderBottom(null)
                    .SetTextAlignment(TextAlignment.CENTER)
                    .Add(new Paragraph(elementFacture.PrixHTFacture.ToString("0.00")).SetMarginBottom(0));
                table.AddCell(cell);

                // Remise
                cell = new Cell()
                    .SetBorderTop(null).SetBorderLeft(null).SetBorderRight(null).SetBorderBottom(null)
                    .SetTextAlignment(TextAlignment.CENTER)
                    .Add(new Paragraph((elementFacture.Remise == 0) ? "" : elementFacture.Remise.ToString("0.00")).SetMarginBottom(0)); ;
                table.AddCell(cell);

                // Prix Unitaire Net HT
                cell = new Cell()
                    .SetBorderTop(null).SetBorderLeft(null).SetBorderRight(null).SetBorderBottom(null)
                    .SetTextAlignment(TextAlignment.CENTER)
                    .Add(new Paragraph(elementFacture.PrixNetHT.ToString("0.00")).SetMarginBottom(0));
                table.AddCell(cell);

                // Montant HT
                cell = new Cell()
                    .SetBorderTop(null).SetBorderLeft(null).SetBorderRight(null).SetBorderBottom(null)
                    .SetTextAlignment(TextAlignment.CENTER)
                    .Add(new Paragraph(elementFacture.MontantHT.ToString("0.00")).SetMarginBottom(0));
                table.AddCell(cell);

                // TVA
                cell = new Cell()
                    .SetBorderTop(null).SetBorderLeft(null).SetBorderRight(null).SetBorderBottom(null)
                    .SetTextAlignment(TextAlignment.CENTER)
                    .Add(new Paragraph(element.TVA.Id.ToString()).SetMarginBottom(0));
                table.AddCell(cell);
            }

            //
            // End of Table
            //
            document.Add(table);

            //
            // Totaux Pièces, Main d'oeuvre, et Forfait
            //

            // Total pièces
            paragraph = new Paragraph("Total pièces : " + facture.TotalPieces.ToString("0.00"))
                .SetMarginBottom(0)
                .SetFontSize(9);

            document.Add(paragraph);
            // Total main d'oeuvre
            paragraph = new Paragraph("Total main d'oeuvre : " + facture.TotalMO.ToString("0.00"))
                .SetMarginTop(0).SetMarginBottom(0)
                .SetFontSize(9);
            document.Add(paragraph);
            // Total forfait
            paragraph = new Paragraph("Total forfait : " + facture.TotalForfait.ToString("0.00"))
                .SetMarginTop(0)
                .SetMarginBottom(40)
                .SetFontSize(9);
            document.Add(paragraph);

            //
            // Garantie
            //

            table = new Table(2)
                .SetWidth(UnitValue.CreatePercentValue(100))
                .SetMarginBottom(6);

            Border border = new SolidBorder(1);
            border.SetColor(ColorConstants.LIGHT_GRAY);

            cell = new Cell()
                .SetTextAlignment(TextAlignment.CENTER)
                .SetBorderTop(null).SetBorderRight(null).SetBorderBottom(border).SetBorderLeft(null)
                .SetPaddingBottom(-6)
                .Add(
                    new Paragraph("Garantie réparation 1 an")
                    .SetBold()
                    .SetFontSize(14)
                    .SetFontColor(ColorConstants.LIGHT_GRAY));
            table.AddCell(cell);

            cell = new Cell()
                .SetTextAlignment(TextAlignment.CENTER)
                .SetBorderTop(null).SetBorderRight(null).SetBorderBottom(border).SetBorderLeft(null)
                .SetPaddingBottom(-6)
                .Add(
                    new Paragraph("Garantie pièces d'origine 1 an")
                    .SetBold()
                    .SetFontSize(14)
                    .SetFontColor(ColorConstants.LIGHT_GRAY));
            table.AddCell(cell);

            document.Add(table);

            //
            // TVA
            //

            table = new Table(9)
                .SetFontSize(8)
                .SetMarginBottom(20);

            //
            // Table Header
            //

            // Code Taxe
            cell = new Cell()
                .SetBorder(null)
                .SetWidth(30)
                .SetPaddingRight(5)
                .SetTextAlignment(TextAlignment.CENTER)
                .Add(new Paragraph("Code Taxe").SetMultipliedLeading(1));
            table.AddCell(cell);

            // Taux T.V.A.
            cell = new Cell()
                .SetBorder(null)
                .SetWidth(30)
                .SetPaddingRight(35)
                .SetTextAlignment(TextAlignment.CENTER)
                .Add(new Paragraph("Taux T.V.A.").SetMultipliedLeading(1));
            table.AddCell(cell);

            // Montant H.T.
            cell = new Cell()
                .SetBorder(null)
                .SetWidth(30)
                .SetPaddingRight(35)
                .SetTextAlignment(TextAlignment.CENTER)
                .Add(new Paragraph("Montant H.T.").SetMultipliedLeading(1));
            table.AddCell(cell);

            // Montant T.V.A.
            cell = new Cell()
                .SetBorder(null)
                .SetWidth(30)
                .SetPaddingRight(35)
                .SetTextAlignment(TextAlignment.CENTER)
                .Add(new Paragraph("Montant T.V.A.").SetMultipliedLeading(1));
            table.AddCell(cell);

            // Montant T.T.C.
            cell = new Cell()
                .SetBorder(null)
                .SetWidth(30)
                .SetPaddingRight(35)
                .SetTextAlignment(TextAlignment.CENTER)
                .Add(new Paragraph("Montant T.T.C.").SetMultipliedLeading(1));
            table.AddCell(cell);

            // Total H.T.
            cell = new Cell()
                .SetBorder(null)
                .SetWidth(30)
                .SetPaddingRight(35)
                .SetTextAlignment(TextAlignment.CENTER)
                .Add(new Paragraph("Total H.T.").SetMultipliedLeading(1));
            table.AddCell(cell);

            // Total T.V.A.
            cell = new Cell()
                .SetBorder(null)
                .SetWidth(30)
                .SetPaddingRight(35)
                .SetTextAlignment(TextAlignment.CENTER)
                .Add(new Paragraph("Total T.V.A.").SetMultipliedLeading(1));
            table.AddCell(cell);

            // Total T.T.C.
            cell = new Cell()
                .SetBorder(null)
                .SetWidth(30)
                .SetPaddingRight(35)
                .SetTextAlignment(TextAlignment.CENTER)
                .Add(new Paragraph("Total T.T.C.").SetMultipliedLeading(1));
            table.AddCell(cell);

            // Total à payer
            cell = new Cell()
                .SetBorder(null)
                .SetWidth(30)
                .SetTextAlignment(TextAlignment.CENTER)
                .Add(new Paragraph("Total à payer").SetMultipliedLeading(1));
            table.AddCell(cell);

            //
            // Table Body
            //

            TVA tva = facture.MontantsHT.FirstOrDefault().Key;

            // Code Taxe
            cell = new Cell()
                .SetBorder(null)
                .SetWidth(30)
                .SetPaddingRight(5)
                .SetTextAlignment(TextAlignment.CENTER)
                .Add(new Paragraph(tva.Id.ToString()));
            table.AddCell(cell);

            // Taux T.V.A.
            cell = new Cell()
                .SetBorder(null)
                .SetWidth(30)
                .SetPaddingRight(35)
                .SetTextAlignment(TextAlignment.CENTER)
                .Add(new Paragraph(tva.Taux.ToString("0.00")));
            table.AddCell(cell);

            // Montant H.T.
            cell = new Cell()
                .SetBorder(null)
                .SetWidth(30)
                .SetPaddingRight(35)
                .SetTextAlignment(TextAlignment.CENTER)
                .Add(new Paragraph(facture.MontantsHT[tva].ToString("0.00")));
            table.AddCell(cell);

            // Montant T.V.A.
            cell = new Cell()
                .SetBorder(null)
                .SetWidth(30)
                .SetPaddingRight(35)
                .SetTextAlignment(TextAlignment.CENTER)
                .Add(new Paragraph((facture.MontantsHT[tva] * tva.Taux / 100).ToString("0.00")));
            table.AddCell(cell);

            // Montant T.T.C.
            cell = new Cell()
                .SetBorder(null)
                .SetWidth(30)
                .SetPaddingRight(35)
                .SetTextAlignment(TextAlignment.CENTER)
                .Add(new Paragraph((facture.MontantsHT[tva] * (1 + tva.Taux / 100)).ToString("0.00")));
            table.AddCell(cell);

            // Total H.T.
            cell = new Cell()
                .SetBorder(null)
                .SetWidth(30)
                .SetPaddingRight(35)
                .SetTextAlignment(TextAlignment.CENTER)
                .Add(new Paragraph(facture.TotalHT.ToString("0.00")));
            table.AddCell(cell);

            // Total T.V.A.
            cell = new Cell()
                .SetBorder(null)
                .SetWidth(30)
                .SetPaddingRight(35)
                .SetTextAlignment(TextAlignment.CENTER)
                .Add(new Paragraph(facture.TotalTVA.ToString("0.00")));
            table.AddCell(cell);

            // Total T.T.C.
            cell = new Cell()
                .SetBorder(null)
                .SetWidth(30)
                .SetPaddingRight(35)
                .SetTextAlignment(TextAlignment.CENTER)
                .Add(new Paragraph(facture.TotalTTC.ToString("0.00")));
            table.AddCell(cell);

            // Total à payer
            cell = new Cell()
                .SetBorder(null)
                .SetWidth(30)
                .SetTextAlignment(TextAlignment.CENTER)
                .Add(new Paragraph(facture.TotalTTC.ToString("0.00")));
            table.AddCell(cell);

            document.Add(table);

            //
            // Net à payer
            //

            table = new Table(2)
                .SetMarginLeft(310)
                .SetMarginBottom(18);

            table.AddCell(new Cell()
                .SetWidth(100)
                .SetBorder(null).SetBorderBottom(border)
                .SetPaddingBottom(-2)
                .Add(new Paragraph("Net à payer (EUR)")
                    .SetFontSize(12)
                    .SetBold()
                    ));

            table.AddCell(new Cell()
                .SetWidth(100)
                .SetBorder(null).SetBorderBottom(border)
                .SetPaddingBottom(-2)
                .Add(new Paragraph(facture.TotalTTC.ToString("0.00"))
                    .SetFontSize(12)
                    .SetBold()
                    .SetTextAlignment(TextAlignment.RIGHT)
                    ));

            document.Add(table);

            //
            // Mentions légales
            //

            Div div = new Div()
                .SetFixedPosition(140, 0, 450)
                .SetFontSize(6f);

            StringBuilder builder = new StringBuilder();

            // Pénalités de retard
            div.Add(new Paragraph(
                builder.Clear()
                    .Append("En application des articles L.441-6 et D.441-5 du code du commerce, ")
                    .Append("des pénalités de retard sont exigibles le jour suivant la date de ")
                    .Append("règlement figurant" + Environment.NewLine)
                    .Append("sur la facture dans le cas où les sommes dues ")
                    .Append("sont payées après cette date. Le taux d'intérêt de ces pénalités ")
                    .Append("est fixé à 0,12%, et l'indemnité forfaitaire à 40 €")
                    .ToString()
                ).SetMultipliedLeading(1).SetMarginBottom(16));

            // Modalités de règlement
            div.Add(new Paragraph(
                builder.Clear()
                    .Append("Voir nos conditions générales de la réparation et de la vente ")
                    .Append("de la pièce de rechange au verso. Conservez ce document, il ")
                    .Append("vous sera demandépour l'application de la garantie." + Environment.NewLine)
                    .Append("T.V.A: acquitée sur les débits." + Environment.NewLine)
                    .Append("MODALITES DE REGLEMENT : Aucun escompte ne sera accordé en cas de ")
                    .Append("paiement ancitipé. En cas de retard de paiement, taux de pénalité ")
                    .Append("appliqué au moins égal à 3 fois le taux d'intérêt légal. Les intérêts ")
                    .Append("de retard de paiement ne seront facturés qu'après l'envoi d'une lettre ")
                    .Append("de contentieux. En cas de défaut de paiement, le Client sera  redevable ")
                    .Append("de plein droit, au titre des frais de recouvrement, de l'indemnité ")
                    .Append("forfaitaire de 40 € prévue par les articles L.441-6 et D.441-5 du Code ")
                    .Append("de commerce sans préjudice de toute indemnité complémentaire en ")
                    .Append("application de l'article L.441-6 du Code de commerce." + Environment.NewLine)
                    .ToString()
                ).SetMultipliedLeading(1).SetMarginBottom(0));

            div.Add(new Paragraph(
                builder.Clear()
                    .Append("Clause de réverse de propriété : nous nous réservons la propriété ")
                    .Append("des marchandises objets des présents débits, jusqu'à leur paiement ")
                    .Append("intégral, conformément à la loi 80-335 du 12 mai 1980.")
                    .ToString())
                .SetFontSize(5.3f)
                .SetBold()
                .SetMultipliedLeading(1)
                .SetMarginTop(0).SetMarginBottom(16));
            document.Add(div);

            //Close document
            document.Close();

            // Open document viewer
            System.Diagnostics.Process.Start(file);

            // Exit Application
            Application.Exit();
        }
    }
}
